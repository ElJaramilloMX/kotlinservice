/**
 * Created by jfuentesj on 4/4/2018.
 */

import com.example.jfuentesj.contacs.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

fun CircleImageView.loadFromUrl(url: String?){
    Picasso.with(this.context).load(url).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).into(this)

}