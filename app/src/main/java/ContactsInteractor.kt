import callback.ContactsCallback
import callback.RetrofitCallback
import models.ApiError
import models.Contact
import models.ResultResponse
import service.ContactService

/**
 * Created by jfuentesj on 4/3/2018.
 */
class ContactsInteractor(private val contactsCallback: ContactsCallback) {

    private val contactService: ContactService by lazy {
        ServiceGenerator.retrofit.create(ContactService::class.java)
    }

    fun getContacs(numberofContacts: Int, gender: String = "") {
        this.contactService.getContacts(numberofContacts, gender).enqueue(object : RetrofitCallback<ResultResponse<Contact>>() {
            override fun onResponseSuccess(response: ResultResponse<Contact>) {
                contactsCallback.onGetContacsResponse(response.results)

            }

            override fun onError(apiError: ApiError) {
                contactsCallback.onError(apiError)
            }
        })
    }

    fun filterContacts(query: String, contact: List<Contact>) {
        if (query.isBlank()){
            contactsCallback.onFilterContactsComplete(contact)
        }else{
            val filteredContacts = contact.filter { contact ->
                val name =  contact.name?.fullName ?: ""
                val email = contact.email
                val cell = contact.cell
                name.contains(query, true) or email.contains(query, true) or cell.contains(query, true)
            }
            contactsCallback.onFilterContactsComplete(filteredContacts)
        }
    }
}