package com.example.jfuentesj.contacs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import viewModel.ContactsViewModel
import android.support.design.widget.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import models.ApiError
import models.Contact
import ContactsPresenter
import adapter.ContactsAdapter
import android.app.SearchManager
import android.content.Context
import android.support.v7.widget.SearchView
import android.view.Menu

class MainActivity : AppCompatActivity(), ContactsViewModel {

    private val presenter: ContactsPresenter by lazy { ContactsPresenter(this) }
    private val contaxtAdapter: ContactsAdapter by lazy { ContactsAdapter(this, mutableListOf()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view_contacs.adapter = contaxtAdapter

        //
        this.refresh_layout.setOnRefreshListener {
            this.presenter.refreshContacts(30)
            this.btn_toggle.isChecked = false
        }

        //
        this.btn_update_button.setOnClickListener{
            this.presenter.updateContacts(30)

        }

        this.btn_toggle.setOnClickListener {
            if (this.btn_toggle.isChecked){
                this.presenter.updateContacts(30,"female")
            }else{
                this.presenter.updateContacts(30,"male")
            }
        }


        //Se manda la peticion al momento que se lanza la actividad
        this.presenter.updateContacts(30)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menuInflater.inflate(R.menu.mane, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as? SearchManager
        val searchView = menu?.findItem(R.id.search)?.actionView as? SearchView

        searchView?.setSearchableInfo(searchManager?.getSearchableInfo(this.componentName))
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?): Boolean {
                val query = newText ?: ""
                presenter.filterContacts(query)
                return query.isNotBlank()
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                val newQuery = query ?: ""
                presenter.filterContacts(newQuery)
                return newQuery.isNotBlank()
            }
        })
        return true
    }

    //Metodo para mostar el error
    override fun displayApiError(apiError: ApiError) {
        val snackbar = Snackbar.make(this.main_container, apiError.error, Snackbar.LENGTH_INDEFINITE )
        snackbar.setAction(android.R.string.ok){
            snackbar.dismiss()
        }
        snackbar.show()

    }

    //
    override fun setProgressVisibility(progressVisibility: Int, emptyMessageVisibility: Int, listVisibility: Int) {
        this.progress_container.visibility = progressVisibility
        this.empy_message_container.visibility = emptyMessageVisibility
        this.refresh_layout.visibility = listVisibility
    }

    //Metodo para hacer el refresh
    override fun setRefreshing(refreshing: Boolean) {
        this.refresh_layout.isRefreshing = refreshing
    }

    //Se usa el adaptador para mostrar los contactos no importa si esta vacio
    override fun displayContacts(contacts: List<Contact>) {
        this.contaxtAdapter.updateContacts(contacts)

    }

}