import android.view.View
import callback.ContactsCallback
import models.ApiError
import models.Contact
import viewModel.ContactsViewModel

/**
 * Created by jfuentesj on 4/3/2018.
 */
class ContactsPresenter(private val contactsViewModel: ContactsViewModel) : ContactsCallback {
    private val interactor: ContactsInteractor by lazy {
        ContactsInteractor(this)
    }

    private var contacts: List<Contact> = emptyList()

    //Metodo para esconder el mensaje de vacio y mostrar el progess aun que la lista este vacia
    fun updateContacts(numberOfContacs: Int, gender: String = "") {
        this.contactsViewModel.setProgressVisibility(
                View.VISIBLE,
                View.INVISIBLE,
                View.VISIBLE
        )
        this.interactor.getContacs(numberOfContacs, gender)
    }

    //Metodo para no mostrar el refresh
    fun refreshContacts(numberOfContacs: Int, gender: String = "") {
        this.contactsViewModel.setProgressVisibility(
                View.INVISIBLE,
                View.INVISIBLE,
                View.VISIBLE
        )
        this.interactor.getContacs(numberOfContacs, gender)
    }

    //
    fun filterContacts(query: String){
        this.interactor.filterContacts(query, this.contacts)
    }


    //Funcion para mostrar los elemntos de la lista aun que este vacia y mostrar el error
    override fun onError(apiError: ApiError) {
        this.contactsViewModel.setProgressVisibility(
                View.INVISIBLE,
                if (this.contacts.isEmpty()) View.VISIBLE else View.INVISIBLE,
                if (this.contacts.isEmpty()) View.INVISIBLE else View.VISIBLE
        )
        this.contactsViewModel.setRefreshing(false)
        this.contactsViewModel.displayApiError(apiError)

    }

    //Se guarda la lista de contactos en una lista local
    override fun onGetContacsResponse(contacs: List<Contact>) {
        this.contacts = contacs
        this.contactsViewModel.setProgressVisibility(
                View.INVISIBLE,
                if (this.contacts.isEmpty()) View.VISIBLE else View.INVISIBLE,
                if (this.contacts.isEmpty()) View.INVISIBLE else View.VISIBLE
        )
        this.contactsViewModel.setRefreshing(false)
        this.contactsViewModel.displayContacts(this.contacts)

    }

    override fun onFilterContactsComplete(contacs: List<Contact>) {
        this.contactsViewModel.displayContacts(contacs)
    }

}