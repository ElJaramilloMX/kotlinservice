package callback

import models.ApiError

/**
 * Created by jfuentesj on 4/3/2018.
 */
interface BaseCallback {
    fun onError(apiError : ApiError)

}