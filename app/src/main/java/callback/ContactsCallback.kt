package callback

import callback.BaseCallback
import models.Contact

/**
 * Created by jfuentesj on 4/3/2018.
 */
interface ContactsCallback : BaseCallback {

    fun onGetContacsResponse(contacs: List<Contact>)

    fun onFilterContactsComplete(contacs: List<Contact>)
}