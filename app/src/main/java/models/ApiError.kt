package models

import com.google.gson.annotations.SerializedName

/**
 * Created by jfuentesj on 4/3/2018.
 */
data class ApiError(@SerializedName("error") private val _error: String?){

    val error:  String
        get() = this._error ?: ""
}