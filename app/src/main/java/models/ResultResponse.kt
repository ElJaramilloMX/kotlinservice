package models

import com.google.gson.annotations.SerializedName

/**
 * Created by jfuentesj on 4/3/2018.
 */
data class ResultResponse<out T>(@SerializedName("results") private val _results: List<T>?) {

    val results: List<T>
        get() =  this._results ?: emptyList()

}