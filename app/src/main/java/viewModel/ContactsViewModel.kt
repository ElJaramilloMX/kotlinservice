package viewModel

import models.Contact
import viewModel.BaseViewModel

/**
 * Created by jfuentesj on 4/3/2018.
 */
interface ContactsViewModel : BaseViewModel {

    fun setProgressVisibility(progressVisibility: Int, emptyMessageVisibility: Int, listVisibility: Int)

    fun setRefreshing(refreshing: Boolean)

    fun displayContacts(contacts: List<Contact>)
}