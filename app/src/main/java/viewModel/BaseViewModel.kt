package viewModel

import models.ApiError

/**
 * Created by jfuentesj on 4/3/2018.
 */
interface BaseViewModel {

    fun displayApiError(apiError: ApiError)

}