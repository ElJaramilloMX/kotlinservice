package adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.jfuentesj.contacs.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import loadFromUrl
import models.Contact

/**
 * Created by jfuentesj on 4/3/2018.
 */
class ContactsAdapter (private val context: Context, private val contacts: MutableList<Contact>): RecyclerView.Adapter<ContactsAdapter.ContactViewHolder>(){

    private val layoutInflater: LayoutInflater by lazy { LayoutInflater.from(this.context) }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ContactViewHolder {
        val view = this.layoutInflater.inflate(R.layout.contac_layout_item, parent, false)
        return ContactViewHolder(view)
    }

    override fun getItemCount(): Int = this.contacts.size

    override fun onBindViewHolder(holder: ContactViewHolder?, position: Int) {
        val contact = this.contacts[position]

        holder?.profileImage?.loadFromUrl(contact.picture?.thumbnail)
        holder?.profileName?.text = contact.name?.fullName
        holder?.profileEmail?.text = contact.email
        holder?.profilePhone?.text = contact.cell

    }

    fun updateContacts(contacts: List<Contact>){
        this.contacts.clear()
        this.contacts.addAll(contacts)
        this.notifyDataSetChanged()

    }

    class  ContactViewHolder(itemView: View?) : RecyclerView.ViewHolder (itemView){

        val profileImage: CircleImageView? = itemView?.findViewById(R.id.profile_image)
        val profileName: TextView? = itemView?.findViewById(R.id.tv_contac_name)
        val profileEmail: TextView? = itemView?.findViewById(R.id.tv_contac_email)
        val profilePhone: TextView? = itemView?.findViewById(R.id.tv_contac_cell_phone_number)
    }
}